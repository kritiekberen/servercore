package nl.kritiekbeer.servercore.commands;

import discord4j.core.object.entity.User;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.lang.languages.Language;
import nl.kritiekbeer.servercore.lang.messages.DiscordIDNotFound;
import nl.kritiekbeer.servercore.lang.messages.InsufficientArguments;
import nl.kritiekbeer.servercore.lang.messages.NowLinking;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.utils.Methods;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LinkDiscordCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        CPlayer cp = Core.playerData.getPlayer(player.getUniqueId());
        Language lang = cp.getLanguage();
        
        if (args.length == 0) {
            sender.sendMessage(lang.getMessage(new InsufficientArguments()));
            return true;
        }
            
        String name = args[0];

        User user;

        try {
            user = Core.utils.getUserByName(name).blockFirst();
        } catch (Exception e) {
            player.sendMessage(cp.getLanguage().getMessage(new DiscordIDNotFound(name)));
            return true;
        }

        if (user == null) {
            sender.sendMessage(lang.getMessage(new DiscordIDNotFound(name)));
            return true;
        } else {
            Core.playerData.getPlayer(player.getUniqueId()).setLinking(true);
            Core.playerData.getPlayer(player.getUniqueId()).setDiscordId(user.getId().toString());
            Core.playerData.getPlayer(player.getUniqueId()).setLinked(false);
            String seed = Methods.getSaltString();
            Core.discordLinkCodes.put((Player)sender, seed);
            Core.utils.sendPrivateMessage(user, "Type the following tekst in your minecraft client ||" + seed + "|| to complete your account linking.");

            Core.playerData.getPlayer(player.getUniqueId()).sendMessage(lang.getMessage(new NowLinking()));
            return true;
        }
    }
}
