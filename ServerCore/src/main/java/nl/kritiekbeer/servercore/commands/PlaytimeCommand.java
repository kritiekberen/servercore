package nl.kritiekbeer.servercore.commands;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.lang.languages.Language;
import nl.kritiekbeer.servercore.lang.messages.PlayerNotFound;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlaytimeCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        
        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;
        CPlayer cp = Core.playerData.getPlayer(player.getUniqueId());
        Language lang = cp.getLanguage();
        String name = "";

        CPlayer target = null;
        if (args.length == 0) {
            target = cp;
        }
        else if (args.length == 1) {
            name = args[0].replace("@", "");
            target = Core.playerData.getDiscordUserOrCPlayer(name.toLowerCase());
        }

        if (target == null) {
            player.sendMessage(lang.getMessage(new PlayerNotFound(name)));
            return true;
        }

        player.sendMessage(target.getName() + "'s playtime is " + target.getPlayTime() + " minutes");
        return true;
    }
}
