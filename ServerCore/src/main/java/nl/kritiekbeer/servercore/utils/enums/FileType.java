package nl.kritiekbeer.servercore.utils.enums;

/**
 * FileType enum
 *
 * @since 1.0.0
 * @author PaulPeriod
 */
public enum FileType {
	CONFIG, 
	USERDATA, 
	ALL;
}
