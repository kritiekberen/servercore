package nl.kritiekbeer.servercore.utils.serializers;

public class BooleanUtils {

	/**
	 * Convert boolean to integer
	 *
	 * @param bool Boolean to convert
	 *
	 * @return 1 (true) / 0 (false)
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static int toInt(boolean bool) {
		return bool ? 1 : 0;
	}

	/**
	 * Convert integer to boolean
	 *
	 * @param bool int (1 / 0) which should be converted to boolean
	 *
	 * @return Converted boolean
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static boolean fromInt(int bool) {
		if (bool == 1) return true;
		return false;
	}
}
