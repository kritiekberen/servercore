package nl.kritiekbeer.servercore.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Methods {

	public static boolean equalsCommand(String argument, List<String> commands) {
		if (commands.contains(argument)) return true;
		return false;
	}

	public static List<String> inColors(List<String> path) {
		List<String> msg = new ArrayList<String>();
		for (String message : path) {
			msg.add(ChatColor.translateAlternateColorCodes('&', message));
		}
		return msg;
	}

	public static String inColors(String path) {
		return "" + ChatColor.translateAlternateColorCodes('&', path);
	}
	
	public static ItemStack getItem(Material material, int damage, int amount, String name, List<String> lore) {
		ItemStack item = new ItemStack(material, amount);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(name);
		meta.setLore(lore);
		
		if (meta instanceof Damageable) {
		    ((Damageable) meta).setDamage(damage);
		}
		
		item.setItemMeta(meta);
		return item;
	}
	
	public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();

        return saltStr;

    }

	/**
	 * Converts a list into a string
	 *
	 * @param list The list to convert into a string
	 * @return Returns a single line string
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
    public static String listToString(List<String> list) {
		String string = "";
		for (String s : list) {
			string += ", " + s;
		}
		string = string.replaceFirst(", ", "");
		return string;
	}
}
