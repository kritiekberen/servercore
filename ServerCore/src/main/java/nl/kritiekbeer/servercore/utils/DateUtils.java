package nl.kritiekbeer.servercore.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

	/**
	 * Add time to a date
	 *
	 * @param old The date to update
	 * @param years Years to add
	 * @param months Months to add
	 * @param days Days to add
	 * @param hours Hours to add
	 * @param minutes Minutes to add
	 *
	 * @return Update date
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static Date add(Date old, int years, int months, int days, int hours, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(old);
		cal.set(cal.get(Calendar.YEAR) + years, cal.get(Calendar.MONTH)+months, cal.get(Calendar.DAY_OF_MONTH)+days, cal.get(Calendar.HOUR_OF_DAY)+hours, cal.get(Calendar.MINUTE)+minutes);
		return cal.getTime();
	}

	/**
	 * Formate a date nicely
	 *
	 * @param date Date to format
	 *
	 * @return Formatted string
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static String format(Date date) {
		int year = getCal(date).get(Calendar.YEAR);
		int month = getCal(date).get(Calendar.MONTH)+1;
		int day = getCal(date).get(Calendar.DAY_OF_MONTH);
		int hour = getCal(date).get(Calendar.HOUR_OF_DAY);
		int minute = getCal(date).get(Calendar.MINUTE);
		
		return (hour+":"+minute+" "+day+"-"+month+"-"+year);
	}

	/**
	 * Get calendar from date
	 *
	 * @param date Date to convert
	 *
	 * @return Converted calendar
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static Calendar getCal(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	/**
	 * Round a date
	 *
	 * @param date Date to round
	 *
	 * @return Rounded date
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static Date round(Date date) {
		Calendar c = new GregorianCalendar();
		c.setTime(date);
		int deltaMin = c.get(Calendar.SECOND)/30;
		
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		c.add(Calendar.MINUTE, deltaMin);
		
		return c.getTime();
	}
} 
