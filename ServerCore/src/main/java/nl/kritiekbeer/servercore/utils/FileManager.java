package nl.kritiekbeer.servercore.utils;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.utils.enums.FileType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class FileManager {

	/**
	 * Save YAML documents
	 *
	 * @param file the FileType to save
	 * @param announce Set if it should be logged in console
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static void saveYamls(FileType file, boolean announce) {
		switch(file) {
		case CONFIG: save(Core.configFile, Core.config, announce); break;
		case USERDATA: save(Core.userdataFile, Core.userdata, announce); break;
		case ALL:
			save(Core.configFile, Core.config, announce);
			save(Core.userdataFile, Core.userdata, announce);
			break;
		}
	}

	/**
	 * Load YAML documents
	 *
	 * @param file the FileType to load
	 * @param announce Set if it should be logged in console
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static void loadYamls(FileType file, boolean announce) {
		switch(file) {
		case CONFIG: load(Core.configFile, Core.config, announce); break;
		case USERDATA: load(Core.userdataFile, Core.userdata, announce); break;
		case ALL:
			load(Core.configFile, Core.config, announce);
			load(Core.userdataFile, Core.userdata, announce);
			break;
		}
	}

	/**
	 * Save a YAML document to a file
	 *
	 * @param file File location
	 * @param conf the YAML file to save
	 * @param announce Set if it should be logged in console
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	private static void save(File file, YamlConfiguration conf, boolean announce) {
		try {
			conf.save(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (announce) Bukkit.getConsoleSender().sendMessage("[" + Core.getPlugin().getDescription().getName() + "] " + ChatColor.DARK_AQUA + "Saved " + file.getName());
	}

	/**
	 * Load a YAML document from a file
	 *
	 * @param file File location
	 * @param conf the YAML to load
	 * @param announce Set if it should be logged in console
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	private static void load(File file, YamlConfiguration conf, boolean announce) {
		try {
			conf.load(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (announce) Bukkit.getConsoleSender().sendMessage("[" + Core.getPlugin().getDescription().getName() + "] " + ChatColor.DARK_AQUA + "Loaded " + file.getName());
	}
	
	
}
