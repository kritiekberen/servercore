package nl.kritiekbeer.servercore.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class LocationUtils {

    /**
     * Convert a location to a string
     *
     * @param location Location to convert
     *
     * @return String of converted location
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public static String locToString(Location location) {
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        String world = location.getWorld().getName();

        return "{" + x + "," + y + "," + z + "," + world + "}";
    }

    /**
     * Convert string to location
     *
     * @param location String to convert
     *
     * @return Location of converted string
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public static Location locFromString(String location) {
        String loc = location.replace("{", "").replace("}", "");
        String[] coords = loc.split(",");
        return new Location(Bukkit.getWorld(coords[3]), Integer.valueOf(coords[0]), Integer.valueOf(coords[1]), Integer.valueOf(coords[2]));
    }

}
