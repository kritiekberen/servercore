package nl.kritiekbeer.servercore.player;

import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerData {

	private List<CPlayer> players;

	/**
	 * PlayerData is a utility for all custom players
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public PlayerData() {
		players = new ArrayList<>();
		load();
	}

	/**
	 * Create a new player
	 *
	 * @param uuid UUID to create a user by
	 *
	 * @return Returns the created CPlayer
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public CPlayer newPlayer(UUID uuid) {
		return playerExists(uuid) ? getPlayer(uuid) : addPlayer(uuid);
	}

	/**
	 * Test if UUID already is used
	 *
	 * @param uuid UUID to test by
	 *
	 * @return Boolean if player exists
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	private boolean playerExists(UUID uuid) {
		for (CPlayer player : getPlayers()) {
			if(player.getUniqueId().equals(uuid)) return true;
		} return false;
	}

	/**
	 * Add a player to the list
	 *
	 * @param uuid UUID of player to add
	 *
	 * @return returns found user or a new created player
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	private CPlayer addPlayer(UUID uuid) {
		CPlayer player = new CPlayer(uuid);
		if (!players.contains(player)) players.add(player);
		return player;
	}

	/**
	 * Get a player by UUID
	 *
	 * @param uuid UUID to find a user by
	 *
	 * @return found CPlayer or null if not found
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public CPlayer getPlayer(UUID uuid) {
		for (CPlayer player : getPlayers()) {
			if (player.getUniqueId().equals(uuid)) return player;
		} return null;
	}

	/**
	 * Get a player by name
	 *
	 * @param name String of username
	 *
	 * @return found CPlayer or null if not found
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public CPlayer getPlayer(String name) {
		for (CPlayer player : getPlayers()) {
			if(player.getName().toLowerCase().equals(name.toLowerCase())) {
				return player;
			}
		} return null;
	}

	/**
	 * Get all online players
	 *
	 * @return List of all online CPlayers
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public List<CPlayer> getOnlinePlayers() {
		List<CPlayer> list = new ArrayList<>();
		for (CPlayer player : getPlayers()) {
			if (player.isOnline()) list.add(player);
		} return list;
	}

	/**
	 * Get all custom players
	 *
	 * @return List of all CPlayers
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public List<CPlayer> getPlayers() {
		return this.players;
	}

	/**
	 * Save all player data
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void save() {
		getPlayers().forEach(player -> {
			player.save();
		});
		Bukkit.getConsoleSender().sendMessage("[" + Core.getPlugin().getDescription().getName() + "] " + ChatColor.DARK_AQUA + "Saved playerdata");
	}

	/**
	 * Load all player data
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void load() {
		getPlayers().forEach(player -> {
			player.load();
		});
		Bukkit.getConsoleSender().sendMessage("[" + Core.getPlugin().getDescription().getName() + "] " + ChatColor.DARK_AQUA + "Loaded player data (file)");
	}

	/**
	 * Get a player by discord ID
	 *
	 * @param id Discord ID to check by
	 *
	 * @return found CPlayer or null if not found
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public CPlayer getPlayerFromDiscord(String id) {
	    for (CPlayer player : getPlayers()) {
	        if (player.getDiscordID().equals(id)) {
	            return player;
	        }
	    } return null;
	}

	/**
	 * Get a CPlayer by discord name or username
	 *
	 * @param name Name to check by
	 *
	 * @return found CPlayer or null if not found
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public CPlayer getDiscordUserOrCPlayer(String name) {
		for (CPlayer player : getPlayers()) {

			String username = Core.discord.getUserById(Snowflake.of(player.getDiscordID())).map(user -> user.getUsername()).block();

			if (username.equalsIgnoreCase(name) || player.getName().equalsIgnoreCase(name)) {
				return player;
			}
		}
		return null;
	}
}
