package nl.kritiekbeer.servercore.player;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.lang.languages.Language;
import nl.kritiekbeer.servercore.utils.Methods;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;

import java.util.HashMap;
import java.util.UUID;

public class CPlayer {
    
    private UUID uuid;
    private OfflinePlayer player;
    private HashMap<String, Object> variables;

    /**
     * Custom player
     *
     * @param uuid UniqueUserID to create the user by
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public CPlayer(UUID uuid) {
        this.uuid = uuid;
        this.player = Bukkit.getOfflinePlayer(uuid);
        variables = new HashMap<>();

        variables.put("name", player.getName());
        variables.put("discordid", "");
        variables.put("language", Core.languages.getLang(Core.config.getString("settings.default_language")).getLanguageName());
        variables.put("linking", false);
        variables.put("linked", false);
        variables.put("playtime", 0);
    }

    /**
     * Get the user's Unique ID
     *
     * @return User's UUID
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public UUID getUniqueId() {
        return this.uuid;
    }

    /**
     * Get the OfflinePlayer
     *
     * @return User's OfflinePlayer
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public OfflinePlayer getPlayer() {
        return this.player;
    }

    /**
     * Update user's player and name
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void update() {
        if (Bukkit.getPlayer(uuid) != null) {
            this.player = Bukkit.getPlayer(uuid);
            this.variables.replace("name", player.getName());
        }
    }

    /**
     * Check if user is online
     *
     * @return Boolean
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public boolean isOnline() {
        return (this.player == null ? false : this.player.isOnline());
    }

    /**
     * Set the name for the user
     *
     * @param name New name
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void setName(String name) {
        this.variables.replace("name", name);
    }

    /**
     * Get the user's name
     *
     * @return String with user's name
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public String getName() {
        return (String) this.variables.get("name");
    }

    /**
     * Send a message to the player
     *
     * @param message The message to send
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void sendMessage(String message) {
        if(isOnline()) player.getPlayer().sendMessage(Methods.inColors(message));
    }

    /**
     * Get the language of the player
     *
     * @return Language of player
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Language getLanguage() {
        return Core.languages.getLang((String) this.variables.get("language"));
    }

    /**
     * Set the user's language
     *
     * @param language New language
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void setLanguage(Language language) {
        this.variables.replace("language", language.getLanguageName());
    }

    /**
     * Set the user's linked discord ID
     *
     * @param ID Discord ID
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void setDiscordId(String ID) {
        this.variables.replace("discordid", ID);
    }

    /**
     * Get user's discord ID
     *
     * @return String with user's discord ID
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public String getDiscordID() {
        return (String) this.variables.get("discordid");
    }

    /**
     * Update player is linking
     *
     * @param linking New linking value
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void setLinking(Boolean linking) {
        this.variables.replace("linking", linking);
    }

    /**
     * Get if player is linking

     * @return Boolean is linking
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public boolean isLinking() {
        return (Boolean) this.variables.get("linking");
    }

    /**
     * Update player is linked
     *
     * @param linked new linked value
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void setLinked(Boolean linked) {
        this.variables.replace("linked", linked);
    }

    /**
     * Get user's linked status
     *
     * @return Boolean is linked
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public boolean isLinked() {
        return (Boolean) this.variables.get("linked");
    }

    /**
     * Teleport a player
     *
     * @param location Location to teleport to
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void teleport(Location location) {
        if (isOnline()) this.player.getPlayer().teleport(location);
    }

    /**
     * Update playtime
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void countPlaytime() {
        this.variables.replace("playtime", getPlayTime()+1);
    }

    /**
     * Get playtime
     *
     * @return minuts player has played (int)
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public int getPlayTime() {
        return (Integer) this.variables.get("playtime");
    }

    /**
     * Set a user variable
     *
     * @param variable identifier for variable
     * @param value the value for the variable
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void setVariable(String variable, Object value) {
        if (!this.variables.containsKey(variable)) {
            this.variables.put(variable, value);
        } else {
            this.variables.replace(variable, value);
        }
    }

    /**
     * Save player data
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void save() {
        for (String identifier : this.variables.keySet()) {
            Core.userdata.set(getUniqueId().toString() + "." + identifier, this.variables.get(identifier));
        }
    }

    /**
     * Load player data
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void load() {
        for (String identifier : Core.userdata.getConfigurationSection(getUniqueId().toString()).getKeys(false)) {
            this.variables.replace(identifier, Core.userdata.get(getUniqueId().toString() + "." + identifier));
        }
    }
}
