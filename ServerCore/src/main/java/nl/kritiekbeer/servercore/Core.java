package nl.kritiekbeer.servercore;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.PresenceUpdateEvent;
import discord4j.core.event.domain.lifecycle.DisconnectEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.entity.User;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.api.CoreAPI;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.interfaces.EventListener;
import nl.kritiekbeer.servercore.bot.listeners.*;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Mentions;
import nl.kritiekbeer.servercore.bot.utils.ModUtils;
import nl.kritiekbeer.servercore.bot.utils.ServerData;
import nl.kritiekbeer.servercore.commands.LinkDiscordCommand;
import nl.kritiekbeer.servercore.commands.MainCommand;
import nl.kritiekbeer.servercore.commands.PlaytimeCommand;
import nl.kritiekbeer.servercore.events.onLinkingChatEvent;
import nl.kritiekbeer.servercore.events.onPlayerChatEvent;
import nl.kritiekbeer.servercore.events.onPlayerJoinEvent;
import nl.kritiekbeer.servercore.lang.LangSetup;
import nl.kritiekbeer.servercore.player.PlayerData;
import nl.kritiekbeer.servercore.utils.FileManager;
import nl.kritiekbeer.servercore.utils.enums.FileType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Core extends JavaPlugin {

    private static Core main;

    public static File configFile;
    public static File userdataFile;
    public static YamlConfiguration config;
    public static YamlConfiguration userdata;

    public static DiscordClient discord;
    public static ServerData sdata;
    public static ModUtils utils;
    public static Mentions mentions;

    public static HashMap<Player, String> discordLinkCodes;

    private File langENFile;

    public static PlayerData playerData;
    public static LangSetup languages;

    public static Mono<TextChannel> botChannel;

    public static CoreAPI api;

    private List<BotCommand> commands;

    public static Core getInstance() {
        return main;
    }

    public static Plugin getPlugin() {
        return main;
    }

    public static List<User> offlineMembers = new ArrayList<>();


    public void onEnable() {
        main = this;
        startup();
        discordLinkCodes = new HashMap<>();

        discord = new DiscordClientBuilder(Constants.token).build();

        botChannel = discord.getChannelById(Snowflake.of(Constants.botChannelId)).ofType(TextChannel.class);


        discord.getEventDispatcher().on(ReadyEvent.class)
                .flatMap(ready -> discord.getChannelById(Snowflake.of(Constants.botChannelId)).ofType(TextChannel.class)
                .flatMap(c -> c.createMessage(":white_check_mark: I'm ready for usage!")))
                .subscribe();

        discord.getEventDispatcher().on(DisconnectEvent.class)
                .flatMap(ready -> discord.getChannelById(Snowflake.of(Constants.botChannelId)).ofType(TextChannel.class)
                .flatMap(c -> c.createMessage(":octagonal_sign:  I just went offline")))
                .subscribe();

        // Custom events
        EventListener<MessageCreateEvent> botCommands = new BotCommands();
        EventListener<MessageCreateEvent> chatInput = new ChatInput();
        EventListener<PresenceUpdateEvent> onlineStatus = new OnlineStatusEvent();
        EventListener<MessageCreateEvent> spamPrevention = new SpamPrevention();
        EventListener<MessageCreateEvent> mentionCreate = new MentionRecieveEvent();

        discord.getEventDispatcher().on(botCommands.getEventType()).subscribe(botCommands::execute);
        discord.getEventDispatcher().on(chatInput.getEventType()).subscribe(chatInput::execute);
        discord.getEventDispatcher().on(onlineStatus.getEventType()).subscribe(onlineStatus::execute);
        discord.getEventDispatcher().on(spamPrevention.getEventType()).subscribe(spamPrevention::execute);
        discord.getEventDispatcher().on(mentionCreate.getEventType()).subscribe(mentionCreate::execute);

        discord.login().block();

        commands = new ArrayList<>();
        sdata = new ServerData();
        utils = new ModUtils();
        mentions = new Mentions();
        api = new CoreAPI();
    }

    public void onDisable() {
        playerData.save();
        FileManager.saveYamls(FileType.USERDATA, false);
        discord.logout();
    }

    private void startup() {
        setupFiles();
        FileManager.loadYamls(FileType.ALL, true);
        registerEvents();
        registerPermissions();
        registerCommands();
        run();

        if (!config.getString("config_version").equals(getDescription().getVersion())) {
            Bukkit.getConsoleSender().sendMessage("[" + getDescription().getName() + "] " + ChatColor.DARK_RED + "Config is outdated, disabling plugin");
            Bukkit.getPluginManager().disablePlugin(this);
        }

        languages = new LangSetup();
        playerData = new PlayerData();
    }

    private void setupFiles() {
        configFile = new File(getDataFolder(), "config.yml");
        userdataFile = new File(getDataFolder(), "userdata.yml");
        langENFile = new File(getDataFolder() + File.separator + "languages", "lang_en.yml");
        firstRun();
        config = new YamlConfiguration();
        userdata = new YamlConfiguration();
    }

    private void registerEvents() {
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new onPlayerJoinEvent(), this);
        pm.registerEvents(new onLinkingChatEvent(), this);
        pm.registerEvents(new onPlayerChatEvent(), this);
    }

    private void registerPermissions() {
        List<String> perms = Arrays.asList(
                "servercore.command.use",
                "servercore.command.language",
                "servercore.command.userinfo",
                "servercore.command.reload",
                "servercore.command.reset",
                "servercore.command.*"
        );
        PluginManager pm = getServer().getPluginManager();
        for (String s : perms) {
            pm.addPermission(new Permission(s));
        }
    }

    private void registerCommands() {
        getCommand("servercore").setExecutor(new MainCommand());
        getCommand("linkdiscord").setExecutor(new LinkDiscordCommand());
        getCommand("playtime").setExecutor(new PlaytimeCommand());
    }

    public void firstRun() {
        try {
            if (!configFile.exists()) {
                configFile.getParentFile().mkdirs();
                copy(getResource("config.yml"), configFile);
            }
            if (!userdataFile.exists()) {
                userdataFile.getParentFile().mkdirs();
                copy(getResource("userdata.yml"), userdataFile);
            }
            if (!langENFile.exists()) {
                langENFile.getParentFile().mkdirs();
                copy(getResource("lang_en.yml"), langENFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDiscordCommand(BotCommand command) {
        if (!this.commands.contains(command)) this.commands.add(command);
    }

    public void removeDiscordCommand(BotCommand command) {
        if (this.commands.contains(command)) this.commands.remove(command);
    }

    public List<BotCommand> getBotCommands() {
        return this.commands;
    }

    public void run() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () ->
                Core.playerData.getOnlinePlayers().forEach(player -> player.countPlaytime()
                ), 1L, (20 * 60));
    }
}
