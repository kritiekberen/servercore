package nl.kritiekbeer.servercore.api;

import discord4j.core.DiscordClient;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.GuildChannel;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.player.CPlayer;
import nl.kritiekbeer.servercore.player.PlayerData;
import reactor.core.publisher.Flux;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.util.UUID;

public class CoreAPI {

    /**
     * Initialize CoreAPI
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public CoreAPI() {}

    /**
     * Get the initiated PlayerData class
     *
     * @return PlayerData class from Core
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    private PlayerData getPlayerData() {
        return Core.playerData;
    }

    /**
     * Get the discord bot
     *
     * @return DiscordClient from Core
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    private DiscordClient getBot() {
        return Core.discord;
    }

    /**
     * Get all channels on the discord server
     *
     * @return HashMap<Name, ID> all channels
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public Flux<GuildChannel> getChannels() {
        return getBot().getGuildById(Snowflake.of(Constants.serverId)).flatMapMany(Guild::getChannels);
    }

//    public Mono<Void> sendToChat() {
//        return Mono.fromRunnable(() -> player.sendMessage("Down below are all discord servers:"))
//                .thenMany(getChannels())
//                .doOnNext(channel -> player.sendMessage(channel.getName() + channel.getId()))
//                .then();
//    }
//    sendToChat().subscribe() to call

    /**
     * Send a message to the discord server
     *
     * @param message Message to send
     * @param channel The ID of the channel where the message should be send
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void sendDiscordMessage(String message, Long channel) {
        Core.discord.getChannelById(Snowflake.of(channel)).ofType(TextChannel.class).flatMap(chan -> chan.createMessage(message)).block();
    }

    /**
     * Send a message to the discord server and delete is later
     *
     * @param message Message to send
     * @param channel The ID of the channel where the message should be send
     * @param delay Interval of deletion in seconds
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void sendDiscordMessageAndDelete(String message, Long channel, int delay) {
        Message m = Core.discord.getChannelById(Snowflake.of(channel)).ofType(TextChannel.class).flatMap(chan -> chan.createMessage(message)).block();
        try { wait(delay); } catch (InterruptedException e) { e.printStackTrace(); }
        m.delete();
    }

    /**
     * Send a message to the discord server with a player mention
     *
     * @param message Message to send
     * @param mention The name of the user to mention
     * @param channel The ID of the channel where the message should be send
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void sendDiscordMentionMessage(String message, String mention, Long channel) {
        IChannel textChannel = getBot().getChannelByID(channel);
        IUser user = getBot().getUsersByName(mention).get(0);
        textChannel.sendMessage(message.replace("%mention%", user.mention()));

        User user = Core
        Core.discord.getChannelById(Snowflake.of(channel)).ofType(TextChannel.class).flatMap(chan -> chan.createMessage(message.replace("%mention%", user.getMention()))).block();
    }

    /**
     * Send a message to the discord server with a player mention and delete it later
     *
     * @param message Message to send
     * @param mention The name of the user to mention
     * @param channel The ID of the channel where the message should be send
     * @param delay Interval of deletion in seconds
     *
     * @return
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void sendDiscordMentionMessageAndDelete(String message, String mention, Long channel, int delay) {
        IChannel textChannel = getBot().getChannelByID(channel);
        IUser user = getBot().getUsersByName(mention).get(0);
        IMessage m = textChannel.sendMessage(message.replace("%mention%", user.mention()));
        try { wait(delay); } catch (InterruptedException e) { e.printStackTrace(); }
        m.delete();
    }

    /**
     * Get a CPlayer by a UUID
     *
     * @param uuid UUID to check
     *
     * @return Found CPlayer or null if not found
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public CPlayer getPlayer(UUID uuid) {
        return getPlayerData().getPlayer(uuid);
    }

    /**
     * Get a CPlayer by name
     *
     * @param name name to check
     *
     * @return Found CPlayer or null if not found
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public CPlayer getPlayer(String name) {
        return getPlayerData().getPlayer(name);
    }

    /**
     * Create a discord command
     *
     * @param command BotCommand to add
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void addDiscordCommand(BotCommand command) {
        Core.getInstance().addDiscordCommand(command);
    }

    /**
     * Remove a discord command
     *
     * @param command BotCommand to remove
     *
     * @since 1.0.0
     * @author PaulPeriod
     */
    public void removeDiscordCommand(BotCommand command) {
        Core.getInstance().removeDiscordCommand(command);
    }
}
