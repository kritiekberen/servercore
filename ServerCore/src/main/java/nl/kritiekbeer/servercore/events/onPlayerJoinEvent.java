package nl.kritiekbeer.servercore.events;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class onPlayerJoinEvent implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		CPlayer player = Core.playerData.newPlayer(e.getPlayer().getUniqueId());

		Location loc = new Location(Bukkit.getWorld(Core.config.getString("settings.first-spawn.world")),
				Core.config.getInt("settings.first-spawn.x"),
				Core.config.getInt("settings.first-spawn.y"),
				Core.config.getInt("settings.first-spawn.z"));

		if (!player.isLinked()) {
			player.teleport(loc);
		}
		
		player.update();
	}
}
