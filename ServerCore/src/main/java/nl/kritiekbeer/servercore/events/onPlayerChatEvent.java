package nl.kritiekbeer.servercore.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.ModUtils;
import nl.kritiekbeer.servercore.player.CPlayer;

public class onPlayerChatEvent implements Listener {
    
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        ModUtils utils = Core.utils;
        CPlayer player = Core.playerData.getPlayer(event.getPlayer().getUniqueId());
        
        if (player.isLinking() || !player.isLinked()) {
            event.setCancelled(true);
            return;
        }
        
        utils.sendMessage(Core.discord.getChannelByID(Long.valueOf(Constants.mcChatChannelId)), event.getMessage(), Core.discord.getUserByID(Long.valueOf(player.getDiscordID())));
    }

}
