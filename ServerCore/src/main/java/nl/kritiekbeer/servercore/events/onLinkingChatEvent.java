package nl.kritiekbeer.servercore.events;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.lang.messages.CanceledLinking;
import nl.kritiekbeer.servercore.lang.messages.DiscordLinked;
import nl.kritiekbeer.servercore.lang.messages.IncorrectLinkCode;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

public class onLinkingChatEvent implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        CPlayer player = Core.playerData.getPlayer(event.getPlayer().getUniqueId());
        if (!player.isLinking()) {
            return;
        }
        
        if (event.getMessage().toLowerCase().equals("cancel")) {
            player.setLinking(false);
            Core.discordLinkCodes.remove(event.getPlayer());
            player.sendMessage(player.getLanguage().getMessage(new CanceledLinking()));
            player.setDiscordId("");
            return;
        }

        event.setCancelled(true);
        String code = event.getMessage();
        if (code.equals(Core.discordLinkCodes.get(event.getPlayer()))) {
            player.setLinking(false);
            Core.discordLinkCodes.remove(event.getPlayer());
            player.setLinked(true);

            IUser user = Core.discord.getUserByID(Long.valueOf(player.getDiscordID()));
            IGuild guild = Core.discord.getChannelByID(Constants.generalTextId).getGuild();
            IRole role = guild.getRoleByID(Constants.memberRoleId);

            IRole[] roles = new IRole[user.getRolesForGuild(guild).size() + 1];
            for (int i = 0; i < user.getRolesForGuild(guild).size(); i++) {
                roles[i] = user.getRolesForGuild(guild).get(i);
            }
            roles[user.getRolesForGuild(guild).size()] = role;

            guild.editUserRoles(user, roles);

            player.sendMessage(player.getLanguage().getMessage(new DiscordLinked(player.getDiscordID())));
        } else {
            player.sendMessage(player.getLanguage().getMessage(new IncorrectLinkCode()));
        }
    }

}
