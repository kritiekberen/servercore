package nl.kritiekbeer.servercore.lang.messages;

public class DiscordIDNotFound extends Message {
    private String target = "none";
    
    public DiscordIDNotFound(String target) {
        this.target = target;
    }
    
    public String getPath() {
        return "discord-id-not-found";
    }
    public String getTarget() {
        return target;
    }
    public String getLanguage() {
        return "";
    }
}
