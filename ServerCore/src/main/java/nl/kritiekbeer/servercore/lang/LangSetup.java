package nl.kritiekbeer.servercore.lang;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.lang.languages.Language;

import java.util.HashMap;

public class LangSetup {
	
	private HashMap<String, Language> languages = new HashMap<String, Language>();

	/**
	 * LangSetup is the storage of languages
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public LangSetup() {
		for (String tag : Core.config.getConfigurationSection("settings.languages").getKeys(false)) {
			languages.put(tag, new Language(tag, Core.config.getString("settings.languages."+tag)));
		}
	}

	/**
	 * Get a language by name
	 *
	 * @param name Name to get by
	 *
	 * @return found Language or null if not found
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public Language getLang(String name) {
		for (Language l : languages.values()) {
			if(l.getLanguageName().toLowerCase().equals(name.toLowerCase())) {
				return l;
			}
		} return null;
	}

	/**
	 * Get all languages
	 *
	 * @return HashMap with all languages (name, language)
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public HashMap<String, Language> get() {
		return this.languages;
	}
}
