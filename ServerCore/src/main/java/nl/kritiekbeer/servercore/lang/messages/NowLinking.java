package nl.kritiekbeer.servercore.lang.messages;

public class NowLinking extends Message {
	private String target = "none";
	
	public NowLinking() {}
	
	public String getPath() {
		return "now-linking";
	}
	public String getTarget() {
		return target;
	}
	public String getLanguage() {
		return "";
	}
}
