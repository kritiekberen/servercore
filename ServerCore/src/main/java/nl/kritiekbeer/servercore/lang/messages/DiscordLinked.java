package nl.kritiekbeer.servercore.lang.messages;

public class DiscordLinked extends Message {
    private String target = "none";
    
    public DiscordLinked(String target) {
        this.target = target;
    }
    
    public String getPath() {
        return "discord-linked";
    }
    public String getTarget() {
        return target;
    }
    public String getLanguage() {
        return "";
    }
}
