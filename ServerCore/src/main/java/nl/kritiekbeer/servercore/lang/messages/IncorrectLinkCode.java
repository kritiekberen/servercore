package nl.kritiekbeer.servercore.lang.messages;

public class IncorrectLinkCode extends Message {
	private String target = "none";
	
	public IncorrectLinkCode() {}
	
	public String getPath() {
		return "incorrect-link-code";
	}
	public String getTarget() {
		return target;
	}
	public String getLanguage() {
		return "";
	}
}
