package nl.kritiekbeer.servercore.lang.messages;

public abstract class Message {

	/**
	 * Get the path to the message
	 *
	 * @return path as String
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public abstract String getPath();

	/**
	 * Get the target in a message
	 *
	 * @return target as String
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public abstract String getTarget();

	/**
	 * Get the language name
	 *
	 * @return language as String
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public abstract String getLanguage();
}
