package nl.kritiekbeer.servercore.lang.messages;

public class CanceledLinking extends Message {
	private String target = "none";
	
	public CanceledLinking() {}
	
	public String getPath() {
		return "canceled-linking";
	}
	public String getTarget() {
		return target;
	}
	public String getLanguage() {
		return "";
	}
}
