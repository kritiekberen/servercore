package nl.kritiekbeer.servercore.lang.languages;

import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.lang.messages.Message;
import nl.kritiekbeer.servercore.utils.Methods;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class Language {

	private File file;
	private YamlConfiguration conf;
	private String tag;
	private String name;

	/**
	 * Language class contains all language data
	 *
	 * @param name Language name
	 * @param tag Language tag
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public Language(String name, String tag) {
		this.tag = tag;
		this.name = name;
		
		this.file = new File(Core.getPlugin().getDataFolder() + File.separator + "languages" + File.separator + "lang_"+this.tag+".yml");
		this.conf = new YamlConfiguration();
		
		if (this.file == null) {
			Bukkit.getConsoleSender().sendMessage("[" + Core.getPlugin().getDescription().getName() + "] " + ChatColor.DARK_RED + "Could not load language " + name + "!");
			return;
		}
		
		try { this.conf.load(this.file);
		} catch (Exception e) {
			e.printStackTrace(); }
		Bukkit.getConsoleSender().sendMessage("[" + Core.getPlugin().getDescription().getName() + "] " + ChatColor.GREEN + "Loaded language " + name + " [" + tag + "] succesfully");
	}

	/**
	 * Return language tag
	 *
	 * @return Language tag
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public String getLanguageTag() {
		return this.tag;
	}
	
	public String getLanguageName() {
		return this.name;
	}

	public String getMessage(Message message) {
		String msg = this.conf.getString(message.getPath());
		if (msg.contains("%target%")) msg = msg.replace("%target%", message.getTarget());
		if (msg.contains("%language%")) msg = msg.replace("%language%", message.getLanguage());
		return Methods.inColors(msg);
	}

	/**
	 * Get language file location
	 *
	 * @return File location
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public File getFile() {
		return this.file;
	}


	/**
	 * Get language configuration file
	 *
	 * @return YamlConfiguration file
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public YamlConfiguration getConf() {
		return this.conf;
	}
}
