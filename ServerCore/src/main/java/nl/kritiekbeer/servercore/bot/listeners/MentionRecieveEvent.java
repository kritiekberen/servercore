package nl.kritiekbeer.servercore.bot.listeners;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.interfaces.EventListener;

public class MentionRecieveEvent implements EventListener<MessageCreateEvent> {

    public Class<MessageCreateEvent> getEventType() {
        return MessageCreateEvent.class;
    }

    public void execute(MessageCreateEvent event) {
        String message = event.getMessage().getContent().get();

        User member = null;

        if (message.contains("@")) {
            for (User m : Core.offlineMembers) {
                if (message.contains(m.getMention())) {
                    member = m;
                }
            }
        }
        if (member == null) return;
        Core.mentions.addMention(member, event.getMessage());
    }

}
