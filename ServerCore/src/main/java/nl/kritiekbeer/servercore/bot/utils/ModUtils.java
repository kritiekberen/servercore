package nl.kritiekbeer.servercore.bot.utils;

import discord4j.core.object.PermissionOverwrite;
import discord4j.core.object.entity.*;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.PermissionSet;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.CMD_Help;
import nl.kritiekbeer.servercore.bot.commands.metrics.*;
import nl.kritiekbeer.servercore.bot.commands.modutils.CMD_Broadcast;
import nl.kritiekbeer.servercore.bot.commands.modutils.CMD_Clear;
import nl.kritiekbeer.servercore.bot.commands.modutils.CMD_Perms;
import nl.kritiekbeer.servercore.bot.commands.modutils.CMD_Stop;
import nl.kritiekbeer.servercore.bot.commands.privatechannel.CMD_Add;
import nl.kritiekbeer.servercore.bot.commands.privatechannel.CMD_Dispose;
import nl.kritiekbeer.servercore.bot.commands.privatechannel.CMD_Private;
import nl.kritiekbeer.servercore.bot.commands.privatechannel.CMD_Remove;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;

public class ModUtils {

	private Category cat;
	private HashMap<User, VoiceChannel> vchannels;

	/**
	 * Utility class for discord
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */

	public ModUtils() {
		cat = Core.discord.getGuildById(Snowflake.of(Constants.serverId)).map(guild -> guild.getChannelById(Snowflake.of(Constants.privateCategoryId))).ofType(Category.class).block();
		vchannels = new HashMap<>();
		
		Core core = Core.getInstance();

		// Help menu
		core.addDiscordCommand(new CMD_Help("help", "Show the help menu", new ArrayList<>()));

		// Metrics
		core.addDiscordCommand(new CMD_8ball("8ball", "Let your question be answered (yes/no)", Arrays.asList("<question>")));
		core.addDiscordCommand(new CMD_Dice("dice", "Roll a dice", Arrays.asList("<amount>")));
		core.addDiscordCommand(new CMD_Fact("fact", "Summon a random fact", new ArrayList<>()));
		core.addDiscordCommand(new CMD_Hello("hello", "Reply with a hello message", new ArrayList<>()));
		core.addDiscordCommand(new CMD_Lastseen("lastseen", "Get past online players", Arrays.asList("<minutes>")));
		core.addDiscordCommand(new CMD_Online("online", "Show all online players on the server", new ArrayList<>()));
		core.addDiscordCommand(new CMD_Playtime("playtime", "Show the player's playtime on the server", Arrays.asList("[mcname/discordname]")));
		
		// Modutils
		core.addDiscordCommand(new CMD_Broadcast("broadcast", "Broadcast a message to channels", Arrays.asList("<channel;>","[e:true/false]")));
		core.addDiscordCommand(new CMD_Clear("clear", "Clear the chat", Arrays.asList("<amount>")));
		core.addDiscordCommand(new CMD_Perms("perms", "help menu for permissions", new ArrayList<>()));
		core.addDiscordCommand(new CMD_Stop("stop", "Stop the bot", new ArrayList<>()));
		
		// private channel
		core.addDiscordCommand(new CMD_Private("private", "Create a private channel", new ArrayList<>()));
		core.addDiscordCommand(new CMD_Remove("remove", "Remove someone from your private channel", Arrays.asList("<mention>")));
		core.addDiscordCommand(new CMD_Add("add", "Add someone to your private channel", Arrays.asList("<mention>")));
		core.addDiscordCommand(new CMD_Dispose("dispose", "Remove your private channel", new ArrayList<>()));
	}

	/**
	 * sends a private message to a user
	 *
	 * @param user The user to send a message to
	 * @param message The message to send
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void sendPrivateMessage(User user, String message) {
	    user.getPrivateChannel().flatMap(u -> u.createMessage(message)).block();
	}

	/**
	 * Create a private voice channel in a guild
	 *
	 * @param owner The owner of the voice channel
	 * @param cat The category in which the channel should be created
	 * @param name The name of the channel
	 * @param userLimit Set the userlimit for the voice channel
	 *
	 * @return Returns the created VoiceChannel
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public Mono<VoiceChannel> createVoiceChannel(User owner, Category cat, String name, int userLimit) {
		Set<PermissionOverwrite> set = new HashSet<>();
		set.add(PermissionOverwrite.forRole(Snowflake.of(Constants.memberRoleId), PermissionSet.none(), PermissionSet.of(Permission.CONNECT)));

		return Core.discord.getGuildById(Snowflake.of(Constants.serverId)).map(guild ->
				guild.createVoiceChannel(spec -> spec.setParentId(cat.getId()).setName(name).setUserLimit(userLimit)
						.setPermissionOverwrites(set)))
						.block();
	}

	/**
	 * Create a text channel in a category
	 *
	 * @param category The category the channel should be created in
	 * @param name The channel's name
	 * @param topic Set the channel's topic
	 *
	 * @return Returns the created TextChannel
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public Mono<TextChannel> createTextChannel(Category category, String name, String topic) {
		return Core.discord.getGuildById(Snowflake.of(Constants.serverId)).map(guild ->
			guild.createTextChannel(spec -> spec.setParentId(category.getId()).setTopic(topic).setName(name)))
				.block();
	}

	/**
	 * Gets the user's private channel
	 *
	 * @param user User to check
	 *
	 * @return Returns found channel or null
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public VoiceChannel getChannelByUser(User user) {
		if (vchannels.containsKey(user)) {
			return vchannels.get(user);
		} return null;
	}

	/**
	 * Deletes the user's private channel
	 *
	 * @param user User to delete it's channel
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void removeVoiceChannel(User user) {
		VoiceChannel c = getChannelByUser(user);
		if (c == null) return;
		vchannels.get(user).delete();
		vchannels.remove(user);
	}

	/**
	 * Remove all private channels on the server
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void removeAllPChannels() {
		cat.getChannels().doOnEach(vc -> vc.get().delete()).subscribe();
	}

	/**
	 * Delete a message with delay
	 *
	 * @param message The message to delete
	 * @param delay The delay in seconds
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void deleteMessageAfter(Message message, int delay) {
        Thread t = new Thread(() -> {
			try {
				Thread.sleep(delay * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			message.delete();
		});
        t.start();
	}

	/**
	 * Get a user by it's name
	 *
	 * @param name Name to check by
	 *
	 * @return found Flux<User> of found users
	 *
	 * @since 1.0.1
	 * @author PaulPeriod
	 */
	public Flux<User> getUserByName(String name) {
		return Core.discord.getUsers().doOnEach(user -> user.get().getUsername().equalsIgnoreCase(name));
	}
}
