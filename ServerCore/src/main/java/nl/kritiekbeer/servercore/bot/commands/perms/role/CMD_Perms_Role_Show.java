package nl.kritiekbeer.servercore.bot.commands.perms.role;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Role;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.Snowflake;
import discord4j.core.spec.EmbedCreateSpec;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;
import reactor.core.publisher.Flux;

import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CMD_Perms_Role_Show {

	public static void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args == 3) {
			// show all roles

			Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate("All roles", Constants.botColor);
			embed.andThen(spec -> event.getGuild().block().getRoles().doOnEach(role -> {
					String color = (role.get().getColor() == null) ? "none" : Utils.fromColor(role.get().getColor());
					spec.addField(role.get().getName(), "roleId: " + role.get().getId().toString() + "\nColor: " + color, false);
				})
			);

			event.getMessage().getChannel().flatMap(channel -> channel.createEmbed(embed));
		}
		else if (args == 4) {
			// show 1 role

			Role role = event.getGuild().block().getRoleById(Snowflake.of(Long.valueOf(msg[3]))).block();

			if (role == null) {
				event.getMessage().getChannel().flatMap(channel -> channel.createMessage("No role found with id " + msg[3]));
				return;
			}

			Flux<Member> membersWithRole = event.getGuild().block().getMembers().doOnEach(member -> member.get().getRoles().filter(uRole -> uRole.equals(role)));
			String members = membersWithRole.map(member -> member.getDisplayName()).collect(Collectors.joining(", ")).block();

			String permissions = "";
			for (Permission permission : role.getPermissions().asEnumSet()) {
				permissions += ", " + permission.name();
			} permissions = permissions.replaceFirst(", ", "");

			Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate(role.getName(), Constants.botColor);
			String finalPermissions = permissions;
			embed.andThen(spec -> spec
				.addField("Role id", role.getId().toString(), false)
				.addField("Users", members, false)
				.addField("Color", (role.getColor() == null) ? "None" : Utils.fromColor(role.getColor()), false)
				.addField("Permissions", finalPermissions, false)
			);

			event.getMessage().getChannel().flatMap(channel -> channel.createEmbed(embed));




//			List<String> members = new ArrayList<>();
//			for (IUser u : event.getGuild().getUsers()) {
//			    if (u.getRolesForGuild(event.getGuild()).contains(role)) members.add(u.getName());
//			}
//
//			String mem = (members.isEmpty()) ? "None" : members.toString().replace("[", "").replace("]", "");
//			EmbedBuilder builder = new EmbedBuilder().withColor(Color.GRAY).withTitle(role.getName());
//			builder.appendField("Role id", role.getStringID(), false);
//			builder.appendField("Users", mem, false);
//			builder.appendField("Color", (role.getColor() == null) ? "None" : Utils.fromColor(role.getColor()), false);
//			builder.appendField("Permissions", role.getPermissions().toString().replace("[", "").replace("]", ""), false);
//			event.getChannel().sendMessage(builder.build());
		}
		else {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
		}
	}
}
