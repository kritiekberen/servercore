package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;

import java.util.List;
import java.util.Random;

public class CMD_8ball extends BotCommand {

    public CMD_8ball(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

	public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	if (args == 1) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("You should ask me a question"));
    		return;
    	}
    	Random random = new Random();
    	int num = random.nextInt( (Constants.ball.size()-1));
    	String result = Constants.ball.get(num);

		event.getMessage().getChannel().flatMap(channel -> channel.createMessage(result));
    }
}
