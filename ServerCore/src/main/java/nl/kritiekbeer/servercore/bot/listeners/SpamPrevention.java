package nl.kritiekbeer.servercore.bot.listeners;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.interfaces.EventListener;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.utils.DateUtils;
import reactor.core.publisher.Mono;

import java.util.Date;
import java.util.HashMap;

public class SpamPrevention implements EventListener<MessageCreateEvent> {

	private HashMap<User, HashMap<Date, Integer>> members = new HashMap<>();

	public Class<MessageCreateEvent> getEventType() {
		return MessageCreateEvent.class;
	}

	public void execute(MessageCreateEvent event) {
		if (event.getMessage().getAuthor().get().isBot()) return;
		if (event.getMessage().getContent().get().startsWith(Constants.botPrefix)) return;
		int max = Constants.maxMessagesPerMinute;
		
		User user = event.getMessage().getAuthor().get();
		HashMap<Date, Integer> map = members.get(user);
		if (map == null) map = new HashMap<>();
		
		Date date = new Date();
		date = DateUtils.round(date);
		int count = 0;
		if (map.containsKey(date)) count = map.get(date);
		count++;
		
		if (count > max) {
			event.getMessage().delete();
			Message m = Mono.zip(event.getMessage().getChannel(), event.getMessage().getAuthorAsMember()).map(mono -> mono.getT1().createMessage(mono.getT2().getMention() + " you are not allowed to send another message, since you are sending to much messages per minute" )).ofType(Message.class).block();
			Core.utils.deleteMessageAfter(m, 10);
		}
		
		if (map.containsKey(date))
			map.replace(date, count);
		if (!map.containsKey(date))
			map.put(date, count);
		if (members.containsKey(user))
			members.replace(user, map);
		if (!members.containsKey(user))
			members.put(user, map);
	}
}
