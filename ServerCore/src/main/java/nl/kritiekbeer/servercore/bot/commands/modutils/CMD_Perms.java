package nl.kritiekbeer.servercore.bot.commands.modutils;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.util.Permission;
import discord4j.core.spec.EmbedCreateSpec;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.commands.perms.role.CMD_Perms_Role_Create;
import nl.kritiekbeer.servercore.bot.commands.perms.role.CMD_Perms_Role_Remove;
import nl.kritiekbeer.servercore.bot.commands.perms.role.CMD_Perms_Role_Show;
import nl.kritiekbeer.servercore.bot.commands.perms.role.CMD_Perms_Role_Update;
import nl.kritiekbeer.servercore.bot.commands.perms.users.CMD_Perms_Users_Addrole;
import nl.kritiekbeer.servercore.bot.commands.perms.users.CMD_Perms_Users_Info;
import nl.kritiekbeer.servercore.bot.commands.perms.users.CMD_Perms_Users_Removerole;
import nl.kritiekbeer.servercore.bot.commands.perms.users.CMD_Perms_Users_Setnick;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class CMD_Perms extends BotCommand {

	public CMD_Perms(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args == 1) {
			Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate("</perms> sub commands", Constants.botColor);
			embed.andThen(embedSpec -> embedSpec
				.addField("Role", "Manager roles", false)
				.addField("Users", "Manage users", false)
				.addField("Permissions", "List all permissions", false)
			);
			event.getMessage().getChannel().flatMap(channel -> channel.createEmbed(embed));
			return;
    	}
    	
    	if (msg[1].equalsIgnoreCase("role")) {
    		if (args == 2) {
				Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate("</perms role> usage", Constants.botColor);
				embed.andThen(embedSpec -> embedSpec
						.addField("Create", "/perms role create <role> [c:color] [m:mentionable] [h:hoisted]", false)
						.addField("Remove", "/perms role remove <roleId>", false)
						.addField("Add", "/perms role add <roleId> <permission>", false)
						.addField("Show", "/perms role show [roleId]", false)
						.addField("Update", "/perms role update <roleId> [c:color] [m:mentionable] [h:hoisted]", false)
				);
				event.getMessage().getChannel().flatMap(channel -> channel.createEmbed(embed));
				return;
    		}
    		
    		// /perm role create <name> [c:color] [m:mentinable]
    		if (msg[2].equalsIgnoreCase("create")) {
    			CMD_Perms_Role_Create.command(event, args, msg, message);
    		}
    		
    		// /perm role remove <roleId>
    		else if (msg[2].equalsIgnoreCase("remove")) {
    			CMD_Perms_Role_Remove.command(event, args, msg, message);
    		}
    		
    		// /perm role show [roleId]
    		else if (msg[2].equalsIgnoreCase("show")) {
    			CMD_Perms_Role_Show.command(event, args, msg, message);
    		}
    		
    		// /perm role update <roleId> [c:color] [m:mentionable] [p:permission;t/f] [h:hoisted]
    		else if (msg[2].equalsIgnoreCase("update")) {
    			CMD_Perms_Role_Update.command(event, args, msg, message);
    		} 

    		else {
				event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Subcommand " + msg[2] + " not found"));
    		}
    	}
    	
    	
    	// /perms users
    	else if (msg[1].equalsIgnoreCase("users")) {
    		if (args == 2) {
				Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate("</perms users> usage", Constants.botColor);
				embed.andThen(embedSpec -> embedSpec
						.addField("Info", "/perms users info <mention>", false)
						.addField("Addrole", "/perms users addrole <mention> <roleId>", false)
						.addField("Removerole", "/perms users removerole <mention> <roleId>", false)
						.addField("Setnick", "/perms users setnick <mention> <newname>", false)
				);
				event.getMessage().getChannel().flatMap(channel -> channel.createEmbed(embed));
    			return;
    		}
    		
    		// /perms users info <mention>
    		else if (msg[2].equalsIgnoreCase("info")) {
    			CMD_Perms_Users_Info.command(event, args, msg);
    		}
    		
    		// /perms users addrole <mention> <roleId>
    		else if (msg[2].equalsIgnoreCase("addrole")) {
    			CMD_Perms_Users_Addrole.command(event, args, msg);
    		}
    		
    		// /perms users removerole <mention> <roleId>
    		else if (msg[2].equalsIgnoreCase("removerole")) {
    			CMD_Perms_Users_Removerole.command(event, args, msg);
    		}
    		
    		// /permms users setnick <mention> <nickname>
    		else if (msg[2].equalsIgnoreCase("setnick")) {
    			CMD_Perms_Users_Setnick.command(event, args, msg);
    		}
    	}
    	
    	
		// /perm permissions
		else if (msg[1].equalsIgnoreCase("permissions")) {
			List<String> perms = new ArrayList<>();

			for (Permission p : Permission.values()){
				perms.add(p.name());
			}
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage(perms.toString().replace("[", "").replace("]", "")));

		}
	}
}
