package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;

import java.util.List;

public class CMD_Hello extends BotCommand {

	public CMD_Hello(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Hello " + event.getMessage().getAuthorAsMember().block().getMention() + ", I'm at your service!"));
	}
}
