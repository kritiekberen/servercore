package nl.kritiekbeer.servercore.bot.commands.perms.users;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Role;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.bot.utils.Utils;

public class CMD_Perms_Users_Addrole {
	
	// /perms users addrole <mention> <roleId>
	public static void command(MessageCreateEvent event, int args, String[] msg) {
		if (args < 5) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
			return;
		}
		String uid = Utils.toId(msg[3]);
		String roleid = msg[4];

		Member member = event.getGuild().block().getMemberById(Snowflake.of(Long.valueOf(uid))).block();
		if (member == null) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Member not valid!"));
			return;
		}

		Role role = event.getGuild().block().getRoleById(Snowflake.of(Long.valueOf(roleid))).block();
		if (role == null) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Role with id " + roleid + " is not valid"));
			return;
		}

		member.removeRole(role.getId());
		event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Removed role from user"));
	}
}
