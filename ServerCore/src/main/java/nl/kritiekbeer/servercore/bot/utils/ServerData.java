package nl.kritiekbeer.servercore.bot.utils;

import discord4j.core.object.entity.User;
import nl.kritiekbeer.servercore.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ServerData {
	
	private HashMap<User, Date> lastSeen = new HashMap<>();

	/**
	 * Add a user to last seen list
	 *
	 * @param member User to add
	 * @param date Date the user went offline
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void addLastSeen(User member, Date date) {
		if (!lastSeen.containsKey(member)) lastSeen.put(member, date);
	}

	/**
	 * Remove a user from the last seen list
	 *
	 * @param member User to remove
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void removeLastSeen(User member) {
		if (lastSeen.containsKey(member)) lastSeen.remove(member);
	}

	/**
	 * Get all users who were online in the given minutes
	 *
	 * @param minutes Minutes to check by
	 *
	 * @return List of users who were online
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public List<User> getLast(int minutes) {
		Date target = DateUtils.add(new Date(), 0, 0, 0, 0, -minutes);
		List<User> members = new ArrayList<>();
		for (User m : lastSeen.keySet()) {
			Date d = lastSeen.get(m);
			if (d.before(target)) continue;
			members.add(m);
		}
		return members;
	}

	/**
	 * Get all last seen users
	 *
	 * @return HashMap of users with their offline date
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public HashMap<User, Date> lastSeenList() {
		return lastSeen;
	}
}
