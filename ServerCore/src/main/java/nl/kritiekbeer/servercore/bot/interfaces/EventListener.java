package nl.kritiekbeer.servercore.bot.interfaces;

import discord4j.core.event.domain.Event;

public interface EventListener<T extends Event> {
    Class<T> getEventType();
    void execute(T event);
}
