package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;

import java.util.List;
import java.util.Random;

public class CMD_Dice extends BotCommand {

	public CMD_Dice(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	int amount = 1;
    	if (args > 1) {
    		try { amount = Integer.valueOf(msg[1]);
    		} catch (NumberFormatException e) {
				event.getMessage().getChannel().flatMap(channel -> channel.createMessage(msg[1] + " is not a valid number"));
        		return; }
    	}
    	
		Random random = new Random();
		int num =  random.nextInt(( (amount * 6) -1) + 1) + 1;
		int finalAmount = amount;
		event.getMessage().getChannel().flatMap(channel -> channel.createMessage(event.getMessage().getAuthorAsMember().block().getMention() + " rolled " + finalAmount + " dice with value: " + num));
	}
}
