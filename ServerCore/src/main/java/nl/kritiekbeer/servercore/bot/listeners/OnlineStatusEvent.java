package nl.kritiekbeer.servercore.bot.listeners;

import discord4j.core.event.domain.PresenceUpdateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.entity.User;
import discord4j.core.object.presence.Presence;
import discord4j.core.object.presence.Status;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.interfaces.EventListener;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;
import org.bukkit.Bukkit;
import reactor.core.publisher.Mono;

import java.util.Date;

public class OnlineStatusEvent implements EventListener<PresenceUpdateEvent> {

	public Class<PresenceUpdateEvent> getEventType() {
		return PresenceUpdateEvent.class;
	}

	public void execute(PresenceUpdateEvent event) {
		User member = event.getUser().block();
		Presence presence = event.getCurrent();
		Status status = presence.getStatus();

		Bukkit.getConsoleSender().sendMessage("Player " + member.getUsername() + " changed his presence to " + status.name());
		
		if (status == Status.ONLINE) {
			Bukkit.getConsoleSender().sendMessage("Player " + member.getUsername() + " is back online");

			Core.sdata.removeLastSeen(member);
			if (!Core.offlineMembers.contains(member) || Core.mentions.getMentions(member) == null) return;
			Core.offlineMembers.remove(member);
			Mono<TextChannel> chan = Core.discord.getChannelById(Snowflake.of(Constants.botChannelId)).ofType(TextChannel.class);

			String times = (Core.mentions.getMentions(member).size() > 1) ? "times" : "time";

			String name = event.getUser().map(u -> u.getUsername()).block();



			Mono<Message> message = chan.flatMap(c -> c.createMessage(messageSpec ->
					messageSpec.setEmbed(Utils.getEmbedTemplate(
							"**" + name + ", while you were offline you were mentioned "
									+ Core.mentions.getMentions(member).size() + times + "**",
							Constants.botColor).andThen(spec -> Core.mentions.getMentions(member).forEach(men -> {
								spec.addField("", "", false);
								spec.addField(men.getAuthorAsMember().map(a -> a.getDisplayName()).block(), men.getContent().get(), false);
							})))));

			if (!Core.mentions.getMentions(member).isEmpty()) {
				Mono<Message> one = Mono.zip(chan, event.getUser()).map(mono -> mono.getT1().createMessage(mono.getT2().getMention())).block();
				Mono<Message> two = chan.flatMap(c -> c.createMessage(m -> message.block()));


				Core.utils.deleteMessageAfter(one.block(), 30);
				Core.utils.deleteMessageAfter(two.block(), 30);
			}
		}
		else {
			if (!Core.offlineMembers.contains(member)) Core.offlineMembers.add(member);
			Core.sdata.addLastSeen(member, new Date());
		}
	}
}
