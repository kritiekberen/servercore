package nl.kritiekbeer.servercore.bot.commands.perms.users;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Role;
import discord4j.core.object.util.Snowflake;
import discord4j.core.spec.EmbedCreateSpec;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CMD_Perms_Users_Info {
	
	public static void command(MessageCreateEvent event, int args, String[] msg) {
		if (args < 4) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
			return;
		}
		
		String id = Utils.toId(msg[3]);
		Member member = event.getGuild().block().getMemberById(Snowflake.of(Long.valueOf(id))).block();

		String roles = member.getRoles().map(Role::getName).collect(Collectors.joining(", ")).block();
		String nick = member.getNickname().orElse("None");

		Consumer<EmbedCreateSpec> spec = Utils.getEmbedTemplate(member.getDisplayName() + "'s info", Constants.botColor).andThen(specs -> specs
				.addField("id", member.getId().toString(), false)
				.addField("roles", roles, false)
				.addField("nickname", nick, false)
				);

		event.getMessage().getChannel().flatMap(channel -> channel.createMessage(specs -> specs.setEmbed(spec)));
	}
}
