package nl.kritiekbeer.servercore.bot.commands.privatechannel;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;

import java.util.List;

public class CMD_Dispose extends BotCommand {

	public CMD_Dispose(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	Core.utils.removeVoiceChannel(event.getMessage().getAuthorAsMember().block());
        Core.utils.sendPrivateMessage(event.getMessage().getAuthorAsMember().block(), "You have removed your private channel");
    }
}
