package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import discord4j.core.spec.EmbedCreateSpec;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;
import nl.kritiekbeer.servercore.utils.DateUtils;

import java.util.List;
import java.util.function.Consumer;

public class CMD_Lastseen extends BotCommand {
	
	public CMD_Lastseen(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    // /lastseen <minutes>
	public void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args != 2) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
			return;
		}
		
		int minutes = 60;
		try {
			minutes = Integer.valueOf(msg[1]);
		} catch (Exception e) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage(msg[1] + " is not a valid number"));
			return;
		}

		Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate("Players who have been online in the last " + minutes + " minutes are:", Constants.botColor);

		int finalMinutes = minutes;
		embed.andThen(embedSpec -> {
			for (User user : Core.sdata.getLast(finalMinutes)) {
				embedSpec.addField("", user.getUsername() + " (" + DateUtils.format(Core.sdata.lastSeenList().get(user)) + ")", false);
			}
		});
		event.getMessage().getChannel().flatMap(spec -> spec.createEmbed(embed));
	}
}
