package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.List;

public class CMD_Online extends BotCommand {

    public CMD_Online(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

	public void command(MessageCreateEvent event, int args, String[] msg, String message) {
        List<String> online = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(player ->
                online.add(player.getName())
        );

        String onlinelist = online.toString().replace("[", "").replace("]", "");
        if (online.isEmpty()) onlinelist = "none";

        String finalOnlinelist = onlinelist;
        event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Current online players: `" + finalOnlinelist + "`"));
    }
}
