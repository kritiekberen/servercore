package nl.kritiekbeer.servercore.bot.commands.modutils;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;

import java.util.List;

public class CMD_Stop extends BotCommand {

	public CMD_Stop(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
		TextChannel channel = event.getGuild().block().getChannelById(Snowflake.of(Constants.botChannelId)).ofType(TextChannel.class).block();
		channel.createMessage(":negative_squared_cross_mark: I just went offline");
		Core.utils.removeAllPChannels();
		event.getClient().logout();
    }
}
