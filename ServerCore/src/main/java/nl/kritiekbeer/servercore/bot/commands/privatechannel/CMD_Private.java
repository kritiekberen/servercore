package nl.kritiekbeer.servercore.bot.commands.privatechannel;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Category;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;

import java.util.List;

public class CMD_Private extends BotCommand {

	public CMD_Private(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
		Category cat = event.getGuild().block().getChannelById(Snowflake.of(Constants.privateCategoryId)).ofType(Category.class).block();
    	Core.utils.createVoiceChannel(event.getMessage().getAuthorAsMember().block(), cat, (event.getMessage().getAuthorAsMember().block() + "'s_channel"), 0);
    	
    	Core.utils.sendPrivateMessage(event.getMessage().getAuthorAsMember().block(), "You have created a private channel (" + (event.getMessage().getAuthorAsMember().block() + "'s_channel") +")");
	}
}
