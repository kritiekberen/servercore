package nl.kritiekbeer.servercore.bot.listeners;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.interfaces.EventListener;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

public class BotCommands implements EventListener<MessageCreateEvent> {

    public Class<MessageCreateEvent> getEventType() {
        return MessageCreateEvent.class;
    }

    public void execute(MessageCreateEvent event) {
        String message = event.getMessage().getContent().get();
        String[] msg = message.split(" ");
        int args = msg.length;

        if (event.getMember().get().isBot()) return;

        event.getMessage().getChannel()
                .filter(c -> !c.equals( Core.discord.getChannelById(
                        Snowflake.of(Constants.botChannelId)).ofType(TextChannel.class) ));

        ArrayList<Mono<TextChannel>> exceptions = new ArrayList<>();
        exceptions.add(Core.discord.getChannelById(Snowflake.of(Constants.mcChatChannelId)).ofType(TextChannel.class));
        exceptions.add(Core.discord.getChannelById(Snowflake.of(Constants.mcConsoleChannelId)).ofType(TextChannel.class));

        if ((event.getMessage().getChannelId().asLong() != Constants.botChannelId )
                && (event.getMessage().getContent().get().startsWith(Constants.botPrefix))
                && (exceptions.contains(event.getMessage().getChannel().ofType(TextChannel.class)))) {

            Mono.zip(event.getMessage().getChannel(), event.getMessage().getAuthorAsMember(), Core.discord.getChannelById(Snowflake.of(Constants.botChannelId)))
                    .flatMap(mono -> mono.getT1().createMessage("Hey " + mono.getT2().getMention() + ", you can control me here: " + mono.getT3().getMention()));

            Message m = Mono.zip(event.getMessage().getChannel(), event.getMessage().getAuthorAsMember(), Core.discord.getChannelById(Snowflake.of(Constants.botChannelId))).flatMap(mono -> mono.getT1().createMessage(mono.getT2().getMention() + ", you can control me here: " + mono.getT3().getMention())).block();
            Core.utils.deleteMessageAfter(m, 30);
        }

        if ((event.getMessage().getChannel().map(c -> c.getId().asLong()).block() != Constants.botChannelId) && (event.getMessage().getContent().get().startsWith(Constants.botPrefix))) {
            Message m = Mono.zip(event.getMessage().getChannel(), event.getMessage().getAuthorAsMember(), Core.discord.getChannelById(Snowflake.of(Constants.botChannelId))).flatMap(mono -> mono.getT1().createMessage(mono.getT2().getMention() + ", you can control me here: " + mono.getT3().getMention())).block();
            Core.utils.deleteMessageAfter(m, 30);
            return;
        }
        
        Thread thread = new Thread(() -> {
            for (BotCommand command : Core.getInstance().getBotCommands()) {
                if (message.toLowerCase().startsWith(Constants.botPrefix + command.getIdentifier())) {
                    command.command(event, args, msg, message);
                }
            }
        });
        thread.start();
                
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
