package nl.kritiekbeer.servercore.bot.commands.perms.role;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Role;
import discord4j.core.object.util.Snowflake;

public class CMD_Perms_Role_Remove {

	public static void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args != 4) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
			return;
		}

		Role role = event.getGuild().block().getRoleById(Snowflake.of(Long.valueOf(msg[3]))).block();
		if (role == null) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Role not found with id " + msg[3]));
			return;
		}

		event.getMessage().getChannel().flatMap(channel -> channel.createMessage("You have removed role (" + role.getName() + ")"));
		role.delete();
	}
}
