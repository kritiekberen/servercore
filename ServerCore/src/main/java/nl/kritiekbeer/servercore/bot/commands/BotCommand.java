package nl.kritiekbeer.servercore.bot.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;

import java.util.List;

public class BotCommand {

    private String identifier, description;
    private List<String> usage;
    
    public BotCommand(String identifier, String description, List<String> usage) {
        this.identifier = identifier;
        this.description = description;
        this.usage = usage;
    }
    
    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
        return;
    }
    
    public String getIdentifier() {
        return this.identifier;
    }
    
    public String getDescription() {
        return this.description;
    }
    
    public List<String> getUsage() {
        return this.usage;
    }
}
