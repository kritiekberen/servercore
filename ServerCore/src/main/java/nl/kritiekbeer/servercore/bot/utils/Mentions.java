package nl.kritiekbeer.servercore.bot.utils;

import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Mentions {
	
	private HashMap<User, List<Message>> map;
	
	public Mentions() {
		map = new HashMap<>();
	}

	/**
	 * Add a mention for a user
	 *
	 * @param user User who is mentioned
	 * @param mention Message where containing the mention
	 */
	public void addMention(User user, Message mention) {
		if (map.containsKey(user)) {
			List<Message> list = map.get(user);
			list.add(mention);
			map.replace(user, list);
		}
		else {
			List<Message> list = new ArrayList<>();
			list.add(mention);
			map.put(user, list);
		}
	}

	/**
	 * Get all mentions from a user
	 *
	 * @param user User to get all mentions by
	 *
	 * @return List of messages containing a mention
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public List<Message> getMentions(User user) {
		return map.get(user);
	}

	/**
	 * Reset all mentions for a user
	 *
	 * @param user User to reset
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public void resetMentions(User user) {
		map.remove(user);
	}
}
