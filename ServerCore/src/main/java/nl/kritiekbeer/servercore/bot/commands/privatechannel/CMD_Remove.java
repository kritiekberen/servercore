package nl.kritiekbeer.servercore.bot.commands.privatechannel;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.PermissionOverwrite;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.VoiceChannel;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.PermissionSet;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CMD_Remove extends BotCommand {

	public CMD_Remove(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	if (args < 2) {
    		event.getMessage().getChannel().flatMap(chan -> chan.createMessage("Please specify a user to add"));
    		return;
    	}
		VoiceChannel vc = Core.utils.getChannelByUser(event.getMessage().getAuthorAsMember().block());
    	if (vc == null) {
    		event.getMessage().getChannel().flatMap(chan -> chan.createMessage("You don't have a channel, use /private"));
    		return;
    	}

		User m = event.getGuild().block().getMemberById(Snowflake.of(Long.valueOf(Utils.toId(msg[1])))).block();

		vc.addMemberOverwrite(m.getId(), PermissionOverwrite.forMember(m.getId(), PermissionSet.none(), PermissionSet.of(Permission.CONNECT)));

		List<User> users = new ArrayList<>();
		vc.getVoiceStates().doOnEach(u -> users.add(u.get().getUser().block()));

		if (users.contains(m)) {
			m.asMember(Snowflake.of(Constants.serverId)).block().edit(spec -> spec.setNewVoiceChannel(vc.getId()));
		}

    	Core.utils.sendPrivateMessage(event.getMessage().getAuthorAsMember().block(), "You have removed " + m.getMention() + " to your channel");
    	Core.utils.sendPrivateMessage(m, "You have been removed from " + event.getMessage().getAuthorAsMember().block().getMention() + "'s channel");
    }
}
