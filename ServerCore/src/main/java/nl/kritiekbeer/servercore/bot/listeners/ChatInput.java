package nl.kritiekbeer.servercore.bot.listeners;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.interfaces.EventListener;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.player.CPlayer;
import org.bukkit.Bukkit;

import java.util.concurrent.ExecutionException;

public class ChatInput implements EventListener<MessageCreateEvent> {

    public Class<MessageCreateEvent> getEventType() {
        return MessageCreateEvent.class;
    }

    public void execute(MessageCreateEvent event) {
        // MCChat
        if (event.getMessage().getChannel().map(c -> c.getId().asLong()).block() == Constants.mcChatChannelId) {
            CPlayer player = Core.playerData.getPlayerFromDiscord(event.getMessage().getAuthorAsMember().map(u -> u.getId().asString()).block());

            String name = event.getMessage().getAuthorAsMember().map(m -> m.getDisplayName()).block();
            if (player != null) {
                name = player.getName();
            }
            Bukkit.getServer().broadcastMessage("§l<" + name + "> §r" + event.getMessage());
        }
        
        // MCConsole
        else if (event.getMessage().getChannel().map(c -> c.getId().asLong()).block() == Constants.mcConsoleChannelId) {
            try {
                Bukkit.getScheduler().callSyncMethod(Core.getPlugin(), () ->
                        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), event.getMessage().toString().replace("/", ""))).get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

    
    }
}
