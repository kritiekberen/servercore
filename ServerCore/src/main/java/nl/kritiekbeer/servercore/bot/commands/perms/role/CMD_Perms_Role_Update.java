package nl.kritiekbeer.servercore.bot.commands.perms.role;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Role;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.awt.*;

public class CMD_Perms_Role_Update {

	// /perm role update <roleId> [c:color] [m:mentionable] [h:hoisted]
	public static void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args < 4) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
			return;
		}
		if (args < 5) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Add an argument to update"));
			return;
		}
		
		Role role = event.getGuild().block().getRoleById(Snowflake.of(Long.valueOf(msg[3]))).block();
		if (role == null) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("No role found with the id " + msg[3]));
			return;
		}

		boolean mentionable = role.isMentionable();
		boolean hoisted = role.isHoisted();

		String c = "";
		String m = ""+mentionable;
		String p = "";
		String h = ""+hoisted;
		for (String s : message.split(" ")) {
			if (s.contains("c:"))
				c = s.split(":")[1];
			if (s.contains("m:"))
				m = s.split(":")[1];
			if (s.contains("h:"))
				h = s.split(":")[1];
		}
		if (!(c.equals("")) && Utils.toColor(c) == null) {
			String finalC = c;
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage(finalC + " is not a valid color"));
		}

		Color color = Utils.toColor(c);
		mentionable = Boolean.valueOf(m);
		hoisted = Boolean.valueOf(h);

		boolean finalMentionable = mentionable;
		boolean finalHoisted = hoisted;
		role.edit(spec -> spec
			.setColor(color)
				.setMentionable(finalMentionable)
				.setHoist(finalHoisted)
		);

		event.getMessage().getChannel().flatMap(channel -> channel.createMessage("You have updated role " + role.getName()));
	}
}