package nl.kritiekbeer.servercore.bot.commands.privatechannel;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.PermissionOverwrite;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.VoiceChannel;
import discord4j.core.object.util.Permission;
import discord4j.core.object.util.PermissionSet;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.util.List;

public class CMD_Add extends BotCommand {

	public CMD_Add(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	if (args < 2) {
    		event.getMessage().getChannel().flatMap(c -> c.createMessage("Please specify a user to add"));
    		return;
    	}
		VoiceChannel vc = Core.utils.getChannelByUser(event.getMessage().getAuthorAsMember().block());
    	if (vc == null) {
    		event.getMessage().getChannel().flatMap(c -> c.createMessage("You don't have a channel, use /private"));
    		return;
    	}

		User m = event.getGuild().block().getMemberById(Snowflake.of(Utils.toId(msg[1]))).block();
    	vc.addMemberOverwrite(m.getId(), PermissionOverwrite.forMember(m.getId(), PermissionSet.of(Permission.CONNECT), PermissionSet.none()));
    	
    	Core.utils.sendPrivateMessage(event.getMessage().getAuthorAsMember().block(), "You have added " + m.getMention() + " to your channel");
    	Core.utils.sendPrivateMessage(m, "You have been added to " + event.getMessage().getAuthorAsMember().block().getMention() + "'s channel");
    }
}
