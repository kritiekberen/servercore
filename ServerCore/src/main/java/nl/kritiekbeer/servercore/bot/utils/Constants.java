package nl.kritiekbeer.servercore.bot.utils;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final String token = "NTUyOTk4OTAzOTg1ODY0NzA0.D2HsAQ.F5KTrx3oRDUH1OKRTuccdlReh-w";
    public static final String botPrefix = "/";
    public static final Long botChannelId = 555411630914142211L;
    public static final Long privateCategoryId = 552999970379399171L;
    public static final Long memberRoleId = 555818456025661442L;
    public static final Long generalTextId = 521681468007907342L;
    public static final Long generalVoiceId = 521681468007907346L;
    public static final Long mcChatChannelId = 554584872862351361L;
    public static final Long mcConsoleChannelId = 554584839555383307L;
    public static final Long serverId = 521681468007907338L;
    public static final boolean statusMessages = true;
    public static int maxMessagesPerMinute = 15;
    private static float[] rgb = Color.RGBtoHSB(176, 137, 104, null);
    public static Color botColor  = Color.getHSBColor(rgb[0], rgb[1], rgb[2]);
    
    public static final List<String> facts = Arrays.asList(
            "Banging your head against a wall burns 150 calories an hour.",
            "A flock of crows is known as a murder.",
            "The person who invented the frisbee, was cremated and made into a frisbee after he died!",
            "An eagle can kill a young deer, and fly away with it.",
            "If you constantly fart for six years & nine months, enough gas is produced to create the energy of an atomic bomb!",
            "A baby octopus is about the size of a flea when it is born.",
            "About 8,000 Americans are injured by music instruments, every year.",
            "A small child could swim through the veins of a blue whale.",
            "An arctophile is a person who collects, or is very fond of teddy bears.",
            "If you leave everything to the last minute, it will only take a minute.",
            "Dying is illegal in the Houses of Parliaments.",
            "Slugs have four noses.",
            "The 20th of March is known as Snowman Burning Day!"
            );
    
    public static final List<String> ball = Arrays.asList(
            "It is certain",
            "It is decided so",
            "Without a doubt",
            "Yes - definitely",
            "You may rely on it",
            "As i see it, yes",
            "Most likely",
            "Outlook good",
            "Yes",
            "Signs points to yes",
            "Reply hazy, try again",
            "Ask again later",
            "Better not tell you now",
            "Cannot predict now",
            "Concentrate and ask again",
            "Don't count on it",
            "My reply is no",
            "My sources say no",
            "Outlook not so good",
            "Very doubtful"
            );
}
