package nl.kritiekbeer.servercore.bot.commands.perms.role;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Role;
import discord4j.core.spec.EmbedCreateSpec;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.awt.*;
import java.util.function.Consumer;

public class CMD_Perms_Role_Create {

	public static void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args < 4) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
			return;
		}
		Color color = Color.GRAY;
		boolean mentionable = false;
		boolean hoisted = false;
		if (args > 4) {
			String c = "";
			String m = "false";
			String h = "false";
			for (String s : message.split(" ")) {
				if (s.contains("c:"))
					c = s.split(":")[1];
				if (s.contains("m:"))
					m = s.split(":")[1];
				if (s.contains("h:"))
					h = s.split(":")[1];
			}
			
			color = Utils.toColor(c);
			if (color == null) {
				event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not a valid color"));
				return;
			}
			mentionable = Boolean.valueOf(m);
			hoisted = Boolean.valueOf(h);
		}
		if (color == null) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Color is not valid"));
			return;
		}

		Color finalColor = color;
		boolean finalMentionable = mentionable;
		Role role = event.getGuild().block().createRole(spec -> spec
				.setColor(finalColor)
				.setName(msg[3])
				.setMentionable(finalMentionable)
		).block();

		Consumer<EmbedCreateSpec> embed = Utils.getEmbedTemplate("", color);


		boolean finalHoisted = hoisted;
		event.getMessage().getChannel().flatMap(channel -> channel.createEmbed(embed.andThen(spec -> spec
				.addField("Name", msg[3], false)
				.addField("Color", Utils.fromColor(finalColor), false)
				.addField("Mentionable", finalMentionable+"", false)
				.addField("Hoisted", finalHoisted +"", false)
		)));
	}
}
