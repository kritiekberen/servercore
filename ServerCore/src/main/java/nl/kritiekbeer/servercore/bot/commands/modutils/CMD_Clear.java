package nl.kritiekbeer.servercore.bot.commands.modutils;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import reactor.core.publisher.Flux;

import java.util.List;

public class CMD_Clear extends BotCommand {

	public CMD_Clear(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
		if (args < 2) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Please specify the number of messages to delete"));
    		return;
    	}
    	int size = 0;
    	try { size = Integer.valueOf(msg[1]);
    	} catch (NumberFormatException e) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage(msg[1] + " is not a valid number"));
    		return; }
    	if (size < 2) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("You must clear more than 2 messages"));
    		return;
    	}
    	if (size > 100) {
			event.getMessage().getChannel().flatMap(channel -> channel.createMessage("You can not clear more than 100 messages"));
    		return;
    	}

    	Flux<Message> messages = event.getMessage().getChannel().map(c -> c.getMessagesBefore(event.getMessage().getId())).block();
		messages.take(size).doOnEach(m -> m.get().delete());

		int finalSize = size;
		Message last = event.getMessage().getChannel().map(c -> c.createMessage("Deleted " + finalSize + " messages")).ofType(Message.class).block();
		Core.utils.deleteMessageAfter(last, 30);
	}
}
