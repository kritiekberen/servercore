package nl.kritiekbeer.servercore.bot.commands;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;
import nl.kritiekbeer.servercore.utils.Methods;
import reactor.core.publisher.Mono;

import java.util.List;

public class CMD_Help extends BotCommand {

    public CMD_Help(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
        Mono<Message> m = event.getMessage().getChannel().flatMap(c -> c.createMessage(messageSpec ->
                messageSpec.setEmbed(Utils.getEmbedTemplate(
                        "Boi Help", Constants.botColor)
                        .andThen(spec -> {
                            for (BotCommand command : Core.getInstance().getBotCommands()) {
                                spec.addField(Constants.botPrefix + command.getIdentifier() + " " + Methods.listToString(command.getUsage()), command.getDescription(), false);
                            }
                        }))));

        Core.utils.deleteMessageAfter(m.block(), 30);
    }
    
}