package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;

import java.util.List;
import java.util.Random;

public class CMD_Fact extends BotCommand {

	public CMD_Fact(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
    	Random random = new Random();
    	int num = random.nextInt( (Constants.facts.size()-1));
    	String fact = Constants.facts.get(num);

    	event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Random fact: **" + fact + "**"));
	}
}
