package nl.kritiekbeer.servercore.bot.commands.metrics;

import discord4j.core.event.domain.message.MessageCreateEvent;
import nl.kritiekbeer.servercore.Core;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.player.CPlayer;

import java.util.List;

public class CMD_Playtime extends BotCommand {

    public CMD_Playtime(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    @Override
    public void command(MessageCreateEvent event, int args, String[] msg, String message) {
        if (args == 0) {
            event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Not enough arguments!"));
            return;
        }

        CPlayer player = null;
        if (args == 1) {
            player = Core.playerData.getPlayerFromDiscord(event.getMessage().getAuthorAsMember().block().getId().toString());
        }
        else if (args == 2) {
            String name = msg[1].replace("@", "");
            player = Core.playerData.getDiscordUserOrCPlayer(name.toLowerCase());
        }

        if (player == null) {
            event.getMessage().getChannel().flatMap(channel -> channel.createMessage("Player not found. Please check any spelling errors."));
            return;
        }

        CPlayer finalPlayer = player;
        event.getMessage().getChannel().flatMap(channel -> channel.createMessage(finalPlayer.getName() + "'s playtime is " + finalPlayer.getPlayTime() + " minutes"));
    }
}
