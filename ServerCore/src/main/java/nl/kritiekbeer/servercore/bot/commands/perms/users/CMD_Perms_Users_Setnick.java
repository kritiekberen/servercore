package nl.kritiekbeer.servercore.bot.commands.perms.users;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.util.Snowflake;
import nl.kritiekbeer.servercore.bot.utils.Utils;

public class CMD_Perms_Users_Setnick {
	
	// /perms users setnick <mention> <nickname>
	public static void command(MessageCreateEvent event, int args, String[] msg) {
		if (args < 5) {
			event.getMessage().getChannel().flatMap(c -> c.createMessage("Not enough arguments!"));
			return;
		}
		
		String uid = Utils.toId(msg[3]);
		Member member;
		try {
			member = event.getGuild().block().getMemberById(Snowflake.of(Long.valueOf(uid))).block();
		}catch (Exception e) {
            event.getMessage().getChannel().flatMap(c -> c.createMessage("Member not valid"));
            return;
		}
		
		String nickname = msg[4];

		member.edit(m -> m.setNickname(nickname));

		event.getMessage().getChannel().flatMap(channel -> channel.createMessage(member.getDisplayName() + "'s nickname has been updated to " + nickname));
	}
}
