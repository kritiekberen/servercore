package nl.kritiekbeer.servercore.bot.utils;

import discord4j.core.spec.EmbedCreateSpec;

import java.awt.*;
import java.util.function.Consumer;

public class Utils {

	/**
	 * Convert a stirng name to a color
	 *
	 * @param color Name of color
	 * @return found Color or null if not found
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static Color toColor(String color) {
		if (color.equalsIgnoreCase("black")) return Color.BLACK;
		if (color.equalsIgnoreCase("blue")) return Color.BLUE;
		if (color.equalsIgnoreCase("cyan")) return Color.CYAN;
		if (color.equalsIgnoreCase("dark_gray")) return Color.DARK_GRAY;
		if (color.equalsIgnoreCase("gray")) return Color.GRAY;
		if (color.equalsIgnoreCase("green")) return Color.GREEN;
		if (color.equalsIgnoreCase("light_gray")) return Color.LIGHT_GRAY;
		if (color.equalsIgnoreCase("magenta")) return Color.MAGENTA;
		if (color.equalsIgnoreCase("orange")) return Color.ORANGE;
		if (color.equalsIgnoreCase("pink")) return Color.PINK;
		if (color.equalsIgnoreCase("red")) return Color.RED;
		if (color.equalsIgnoreCase("white")) return Color.WHITE;
		if (color.equalsIgnoreCase("yellow")) return Color.YELLOW;
		return null;
	}

	/**
	 * Convert a color to a string
	 *
	 * @param color Color instance to convert
	 * @return String of color
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static String fromColor(Color color) {
		if (color.equals(Color.BLACK)) return "Black";
		if (color.equals(Color.BLUE)) return "Blue";
		if (color.equals(Color.CYAN)) return "Cyan";
		if (color.equals(Color.DARK_GRAY)) return "Dark Gray";
		if (color.equals(Color.GRAY)) return "Gray";
		if (color.equals(Color.GREEN)) return "Green";
		if (color.equals(Color.LIGHT_GRAY)) return "Light Gray";
		if (color.equals(Color.MAGENTA)) return "Magenta";
		if (color.equals(Color.ORANGE)) return "Orange";
		if (color.equals(Color.PINK)) return "Pink";
		if (color.equals(Color.RED)) return "Red";
		if (color.equals(Color.WHITE)) return "White";
		if (color.equals(Color.YELLOW)) return "Yellow";
		return "none";
	}

	/**
	 * Convert a discord mention to an Id
	 *
	 * @param string String to decypher
	 * @return String of id
	 *
	 * @since 1.0.0
	 * @author PaulPeriod
	 */
	public static String toId(String string) {
		return string.replace("<", "").replace(">", "").replace("!", "").replace("@", "").replace("#", "");
	}

	/**
	 * Get the template for an embed message
	 *
	 * @param name Title of the embed message
	 * @param color Color of the embed message
	 *
	 * @return Converted Consumer<EmbedCreateSpec>
	 *
	 * @since 1.0.1
	 * @author PaulPeriod
	 */
	public static Consumer<EmbedCreateSpec> getEmbedTemplate(String name, Color color) {
		return spec -> spec.setColor(color).setTitle(name);
	}
}
