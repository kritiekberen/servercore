package nl.kritiekbeer.servercore.bot.commands.modutils;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.TextChannel;
import discord4j.core.object.util.Snowflake;
import discord4j.core.spec.EmbedCreateSpec;
import nl.kritiekbeer.servercore.bot.commands.BotCommand;
import nl.kritiekbeer.servercore.bot.utils.Constants;
import nl.kritiekbeer.servercore.bot.utils.Utils;

import java.util.List;
import java.util.function.Consumer;

public class CMD_Broadcast extends BotCommand {
    
	public CMD_Broadcast(String identifier, String description, List<String> usage) {
        super(identifier, description, usage);
    }

    // /broadcast <channel;> [e:t/f] <message>
	public void command(MessageCreateEvent event, int args, String[] msg, String message) {
		String send = "";
		String[] words = message.split(" ");

		Consumer<EmbedCreateSpec> spec = Utils.getEmbedTemplate("", Constants.botColor);
		
		int start = 2;
		boolean e = false;
		for (String s : words) {
			if (s.contains("e:")) {
				e = Boolean.valueOf(s.split(":")[1]);
				start = 3;
			}
		}
		
		for (int i = start; i < words.length; i++)
			send += " " + words[i];

		String finalSend = send;
		spec.andThen(s -> s.addField("Broadcast", finalSend, false));

		String[] channels = msg[1].split(";");
		for (String s : channels) {
			TextChannel chan = event.getGuild().block().getChannelById(Snowflake.of(Long.valueOf(Utils.toId(s)))).ofType(TextChannel.class).block();
			if (!e) chan.createMessage(send);
			if (e) chan.createMessage(m -> m.setEmbed(spec));
		}
		
	}

}
